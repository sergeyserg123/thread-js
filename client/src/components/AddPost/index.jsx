import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const AddPost = ({
  addPost,
  updatePost,
  editablePost,
  resetEditablePost,
  uploadImage
}) => {
  const [body, setBody] = useState('');
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);
  const [isEditMode, setEditMode] = useState(false);

  useEffect(() => {
    if (editablePost) {
      const { body: editableBody, image: editableImage } = editablePost;

      if (editableBody !== undefined) {
        setBody(editableBody);
      }

      if (editableImage) {
        const { id: imageId, link: imageLink } = editableImage;
        setImage({ imageId, imageLink });
      }
      setEditMode(true);
    }
  }, [editablePost]);

  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    await addPost({ imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
  };

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    const editablePostId = editablePost.id;
    await updatePost({ id: editablePostId, imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
    setEditMode(false);
    resetEditablePost();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={isEditMode ? handleUpdatePost : handleAddPost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">{isEditMode ? 'Update' : 'Post'}</Button>
      </Form>
    </Segment>
  );
};

AddPost.propTypes = {
  addPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  editablePost: PropTypes.instanceOf(Object),
  resetEditablePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

AddPost.defaultProps = {
  editablePost: undefined
};

export default AddPost;
