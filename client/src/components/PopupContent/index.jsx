/* eslint-disable  */
import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';
import { getUserImgLink } from '../../helpers/imageHelper';

import styles from './styles.module.scss';

const PopupContent = ({ postReactions }) =>
(
    <div className={styles.popupRoot}>
        {postReactions.map((reaction, index) => (
            <Image className={styles.image} key={index} src={getUserImgLink(reaction.user.image)} size='mini' circular />)
        )}
    </div>
);


PopupContent.propTypes = {
    postReactions: PropTypes.arrayOf(PropTypes.any).isRequired
};

export default PopupContent;