/* eslint-disable */
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Modal, Input, Button, Grid } from 'semantic-ui-react';

const ResetPassword = ({ isResetMode, isAllowResetPassword, isResetMessageSent, isLoading, onTryAllowReset, onSavePassword }) => {
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const onChangePassword = (e) => {
        setPassword(e.target.value);
    };

    const onChangeConfirmPassword = (e) => {
        setConfirmPassword(e.target.value);
    };

    const savePassword = () => {
        if (password === confirmPassword) {
            onSavePassword(password);
        }
    };

    return (
        <Modal.Content>
            <Grid container textAlign="center" style={{ paddingTop: 30 }}>
                <Grid.Column>
                    {(isAllowResetPassword && isResetMode) && (
                        <>
                            <Input
                                placeholder="Password"
                                type="password"
                                onChange={onChangePassword}
                                value={password}
                            />
                            <br />
                            <br />
                            <Input
                                placeholder="Confirm password"
                                type="password"
                                onChange={onChangeConfirmPassword}
                                value={confirmPassword}
                            />
                            <br />
                            <br />
                            <Button type="button" color="teal" size="large" primary loading={isLoading} disabled={isLoading} onClick={savePassword}>
                                Save password
                            </Button>
                        </>
                    )}
                    {(!isAllowResetPassword && !isResetMessageSent && isResetMode) && (
                        <Button type="button" color="red" size="large" loading={isLoading} disabled={isLoading} onClick={() => onTryAllowReset()}>
                            Confirm reset
                        </Button>
                    )}

                    {(isResetMessageSent && isResetMode) && (
                        <p>We have sent the reset link to your email address!</p>
                    )}
                </Grid.Column>
            </Grid>
        </Modal.Content>
    )
};

ResetPassword.propTypes = {
    isResetMode: PropTypes.bool.isRequired,
    isAllowResetPassword: PropTypes.bool.isRequired,
    isResetMessageSent: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    onTryAllowReset: PropTypes.func.isRequired,
    onSavePassword: PropTypes.func.isRequired
};

export default ResetPassword;