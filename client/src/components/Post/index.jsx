/* eslint-disable  */
import React from 'react';
import PropTypes from 'prop-types';
import { Card, Label, Icon, Popup, Image } from 'semantic-ui-react';
import moment from 'moment';
import PopupContent from '../PopupContent/index';

import styles from './styles.module.scss';


const Post = ({ authorId, post, likePost, dislikePost, toggleExpandedPost, sharePost, editPost, softRemovePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    postReactions,
    createdAt
  } = post;

  const showBtns = () => user.id === authorId;

  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        {postReactions.length > 0 ? (
          <Popup 
            content={<PopupContent postReactions={postReactions} />}
            trigger={
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
              <Icon name="thumbs up" />
                {likeCount}
            </Label>}
          />
        ) : (
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
            <Icon name="thumbs up" />
              {likeCount}
          </Label>
        )}
        
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {showBtns() && 
        <Label basic size="small" as="a" className={`${styles.toolbarBtn}`} onClick={() => editPost(id)} >
          <Icon name="edit" />
        </Label>}
        {showBtns() && 
        <Label basic size="small" as="a" className={`${styles.toolbarBtn} ${styles.revomeBtn}`} onClick={() => softRemovePost(id)} >
          <Icon name="remove" />
        </Label>}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  authorId: PropTypes.string.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func,
  softRemovePost: PropTypes.func.isRequired
};

export default Post;
