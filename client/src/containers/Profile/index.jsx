import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Button,
  Modal,
  Icon
} from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';
import ReactCrop from 'react-image-crop';
import { updateUser, tryAllowResetPassword, resetPassword, changePassword } from './actions';
import 'react-image-crop/dist/ReactCrop.css';

import styles from './styles.module.scss';
import ResetPassword from '../../components/ResetPassword';

const cropInitial = {
  unit: 'px',
  width: 200,
  height: 200,
  aspect: 1 / 1
};

/* eslint-disable */
const Profile = ({
  user,
  updateUser,
  tryAllowResetPassword,
  resetPassword,
  changePassword,
  isAllowResetPassword,
  isResetMessageSent,
  router,
  history
}) => {
  const [isEditMode, setEditMode] = useState(false);
  const [isResetMode, setResetMode] = useState(false);
  const [isOpenModal, setOpenModal] = useState(false);
  const [file, setFile] = useState(undefined);
  const [src, setSrc] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [crop, setCrop] = useState(cropInitial);
  const [image, setImage] = useState(undefined);
  const [username, setUsername] = useState(user.username);
  const [status, setStatus] = useState(user.status);

  useEffect(() => {
    imageRef = image;
    const { resetCode } = router.location.query;
    if (resetCode) {
      setResetMode(true);
      onResetPassword({ resetCode });
    }
    setStatus(user.status);
  }, [user]);

  let imageRef = undefined;
  let fileUrl = undefined;

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () =>
        setSrc(reader.result)
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onImageLoaded = image => {
    setImage(image);
    imageRef = image;
  };

  const onCropComplete = crop => {
    makeClientCrop(crop);
  };

  const onCropChange = crop => {
    setCrop(crop);
  };

  const makeClientCrop = async (crop) => {
    if (imageRef && crop.width && crop.height) {
      cropImg(
        imageRef,
        crop,
        'avatar.jpeg'
      );
    }
  };

  const cropImg = (image, crop, fileName) => {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    canvas.toBlob(blob => {
      if (!blob) {
        return;
      }
      blob.name = fileName;
      setFile(blob);
      window.URL.revokeObjectURL(fileUrl);
      fileUrl = window.URL.createObjectURL(blob);
    })
  };

  const toggleProfileMode = () => {
    if (isEditMode && (user.username !== username || user.status !== status)) {
      onSubmit();
    }
    setEditMode(!isEditMode);
  };

  const onChangeUserName = (e) => {
    setUsername(e.target.value);
  };

  const onChangeStatus = (e) => {
    const { value } = e.target;
    if (value.length <= 14) {
      setStatus(value);
    }
  };

  const onResetPassword = async resetCode => {
    await resetPassword(resetCode);
  };

  const onSubmit = async () => {
    setIsLoading(true);
    try {
      const updatedUser = { ...user, username, status };
      var result = await updateUser(updatedUser);
    } finally {
      result ? null : setUsername(user.username) && setStatus(user.status);
      setIsLoading(false);
    };
  };

  const cancelModal = () => {
    setOpenModal(false);
    setSrc(undefined);
  };

  const openModal = () => {
    setOpenModal(true);
  };

  const handleUploadFile = async () => {
    setIsLoading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(file);
      const updatedUser = { ...user, imageId, imageLink };
      updateUser(updatedUser);
    } finally {
      // TODO: show error
      cancelModal();
      setIsLoading(false);
    }
  };

  const toggleResetMode = () => {
    setResetMode(!isResetMode);
  };

  const tryAllowReset = async () => {
    setIsLoading(true);
    try {
      await tryAllowResetPassword();
    } finally {
      setIsLoading(false);
    }
  };

  const savePassword = async password => {
    try {
      const result = await changePassword({ password });
      if (result) {
        toggleResetMode();
      }
    } finally { 
      history.push('/profile');
      setResetMode(false);
    }
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (

    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image centered onClick={openModal} src={getUserImgLink(user.image)} size="medium" circular className={styles.avatar} />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          onChange={onChangeUserName}
          disabled={!isEditMode}
          value={username}
        />
        <br />
        <br />
        <Input
          icon="tag"
          iconPosition="left"
          placeholder="Status"
          type="text"
          onChange={onChangeStatus}
          value={status}
          disabled={!isEditMode}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        
        <Button type="button" color="teal" size="large" primary loading={isLoading} disabled={isLoading} onClick={toggleProfileMode}>
          {!isEditMode ? 'Edit' : 'Save'}
        </Button>
        <Button type="button" color="red" size="large" loading={isLoading} disabled={isLoading} onClick={toggleResetMode}>
          Reset password
        </Button>
      </Grid.Column>

      <Modal
        open={isOpenModal}
      >
        <Modal.Header>Upload avatar</Modal.Header>
        <Modal.Content>
          {src && (
            <ReactCrop
              src={src}
              crop={crop}
              circularCrop
              maxHeight="200"
              maxWidth="200"
              minHeight="100"
              minWidth="100"
              ruleOfThirds
              onImageLoaded={onImageLoaded}
              onComplete={onCropComplete}
              onChange={onCropChange}
            />
          )}
          {!src && (
            <Button color="teal" icon as="label" loading={isLoading} disabled={isLoading}>
              <Icon name="image" />
              Attach image
              <input name="image" type="file" onChange={onSelectFile} hidden />
            </Button>
          )}
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={cancelModal} negative disabled={isLoading}>
            Cancel
          </Button>

          {src && (
            <Button onClick={handleUploadFile} loading={isLoading} disabled={isLoading} positive>
              Upload
            </Button>
          )}

        </Modal.Actions>
      </Modal>

      <Modal
        open={isResetMode}
        content={<ResetPassword
          isResetMode={isResetMode}
          isLoading={isLoading}
          isAllowResetPassword={isAllowResetPassword}
          isResetMessageSent={isResetMessageSent}
          onTryAllowReset={tryAllowReset}
          onSavePassword={savePassword} />}
      >
      </Modal>
    </Grid>
  )
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUser: PropTypes.func.isRequired,
  changePassword: PropTypes.func.isRequired,
  tryAllowResetPassword: PropTypes.func.isRequired,
  isResetMessageSent: PropTypes.bool,
  isAllowResetPassword: PropTypes.bool
};

Profile.defaultProps = {
  user: {},
  isAllowResetPassword: false,
  isResetMessageSent: false
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user,
  isResetMessageSent: rootState.profile.isResetMessageSent,
  isAllowResetPassword: rootState.profile.isAllowResetPassword,
  router: rootState.router
});

const actions = {
  updateUser,
  tryAllowResetPassword,
  resetPassword,
  changePassword
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
