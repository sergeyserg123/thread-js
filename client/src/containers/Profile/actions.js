import * as authService from 'src/services/authService';
import {
  SET_USER,
  TRY_ALLOW_RESET_USER_PASSWORD,
  ALLOW_RESET_USER_PASSWORD,
  RESET_USER_PASSWORD_DEFAULT
} from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const setResetMessageSent = isResetMessageSent => async dispatch => dispatch({
  type: TRY_ALLOW_RESET_USER_PASSWORD,
  isResetMessageSent
});

const setAllowResetPassword = isAllowResetPassword => async dispatch => dispatch({
  type: ALLOW_RESET_USER_PASSWORD,
  isAllowResetPassword
});

const setResetPasswordDefault = () => async dispatch => dispatch({
  type: RESET_USER_PASSWORD_DEFAULT
});

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const updateUser = request => async (dispatch, getRootState) => {
  const user = await authService.updateUser(request);
  if (user) setUser(user)(dispatch, getRootState);
  return Boolean(user);
};

export const tryAllowResetPassword = () => async (dispatch, getRootState) => {
  const isResetMessageSent = await authService.tryAllowResetPass();
  if (isResetMessageSent) setResetMessageSent(isResetMessageSent)(dispatch, getRootState);
};

export const resetPassword = request => async (dispatch, getRootState) => {
  const isAllowResetPassword = await authService.resetPassword(request);
  setAllowResetPassword(isAllowResetPassword)(dispatch, getRootState);
};

export const changePassword = request => async (dispatch, getRootState) => {
  const result = await authService.changePassword(request);
  if (result) setResetPasswordDefault()(dispatch, getRootState);
  return Boolean(result);
};
