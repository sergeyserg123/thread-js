import {
  SET_USER,
  TRY_ALLOW_RESET_USER_PASSWORD,
  ALLOW_RESET_USER_PASSWORD,
  RESET_USER_PASSWORD_DEFAULT
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false,
        isResetMessageSent: false,
        isAllowResetPassword: Boolean(action.user?.isAllowResetPassword)
      };
    case TRY_ALLOW_RESET_USER_PASSWORD:
      return {
        ...state,
        isResetMessageSent: action.isResetMessageSent
      };
    case ALLOW_RESET_USER_PASSWORD:
      return {
        ...state,
        isResetMessageSent: false,
        isAllowResetPassword: Boolean(action.isAllowResetPassword)
      };
    case RESET_USER_PASSWORD_DEFAULT:
      return {
        ...state,
        isResetMessageSent: false,
        isAllowResetPassword: false
      };
    default:
      return state;
  }
};
