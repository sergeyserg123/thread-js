import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  UPDATE_POST,
  SOFT_REMOVE_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const updatePostAction = posts => ({
  type: UPDATE_POST,
  posts
});

const softDeletePostAction = posts => ({
  type: SOFT_REMOVE_POST,
  posts
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const softRemovePost = postId => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  await postService.softDeletePost(postId);
  const updatedPosts = posts.filter(post => post.id !== postId);
  dispatch(softDeletePostAction(updatedPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

/* eslint-disable no-confusing-arrow */
export const updatePost = post => async (dispatch, getRootState) => {
  const { id } = await postService.updatePost(post);
  const updatedPost = await postService.getPost(id);
  const { posts: { posts } } = getRootState();
  const updatedPosts = posts.map(p => p.id === updatedPost.id ? updatedPost : p);
  dispatch(updatePostAction(updatedPosts));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

// TODO: dispatch new reaction to post after like
export const likePost = postId => async (dispatch, getRootState) => {
  const { reaction, dislikeDeleted } = await postService.likePost(postId);
  const diff = reaction ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const dislikeDiff = dislikeDeleted ? 1 : 0;

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) - dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();

  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { reaction, likeDeleted } = await postService.dislikePost(postId);
  const diff = reaction ? 1 : -1;
  const likeDiff = likeDeleted ? 1 : 0;

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff,
    likeCount: Number(post.likeCount) - likeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
