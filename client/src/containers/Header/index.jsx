import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateUser } from '../Profile/actions';

import styles from './styles.module.scss';

const Header = ({ user, logout, updateUser: update }) => {
  const [isEditMode, setEditMode] = useState(false);
  const [status, setUserStatus] = useState(user.status);

  useEffect(() => {
    setUserStatus(user.status);
  }, [user]);

  const onChangeUserStatus = e => {
    const { value } = e.target;
    if (value.length <= 14) {
      setUserStatus(e.target.value);
    }
  };

  /* eslint-disable */
  const onSubmit = async () => {
    try {
      const updatedUser = { ...user, status };
      var result = await update(updatedUser);
    } finally {
      if (result) setUserStatus(user.status);
    }
  };

  const toggleEditMode = e => {
    const { id } = e.target;
    if (id !== 'editBtn' && id !== 'editInput') return;
    e.preventDefault();

    if (id === 'editBtn') {
      if (isEditMode && user.status !== status) {
        onSubmit();
      }
      setEditMode(!isEditMode);
    }   
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <HeaderUI>
              <NavLink exact to="/" onClick={toggleEditMode}>
                <div className={styles.userDataContainer}>
                  <Image circular src={getUserImgLink(user.image)} className={styles.headerImage} />
                  <div className={styles.userData}>
                    <div className={styles.userName}>
                      {user.username}
                    </div>
                    <div className={styles.userStatus}>
                      <input id="editInput" disabled={!isEditMode} onChange={onChangeUserStatus} value={status} />
                      <Icon id="editBtn" name={isEditMode ? 'save' : 'edit'} className={styles.editIcon} />
                    </div>
                  </div>
                </div>
              </NavLink>
            </HeaderUI>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  logout: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired  
};

Header.defaultProps = {
  user: {}
};

const actions = {
  updateUser
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
