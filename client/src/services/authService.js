import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const updateUser = async request => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'PUT',
      request
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const tryAllowResetPass = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/reset-password',
      type: 'post'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const resetPassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/confirm-reset-password',
    type: 'post',
    request
  });
  return response.json();
};

export const changePassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/change-password',
    type: 'post',
    request
  });
  return response.json();
};
