#### Common
1. ES2020
2. [Git](https://git-scm.com/book/ru/v1/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git "Git")
3. [REST API](https://www.restapitutorial.com/lessons/restquicktips.html "REST API")
4. [JWT](https://en.wikipedia.org/wiki/JSON_Web_Token "JWT")
5. [Socket.IO](https://socket.io/docs/ "Socket.IO")
6. [npm](https://en.wikipedia.org/wiki/Npm_(software))
7. [ESLint](https://eslint.org/docs/user-guide/getting-started "ESLint")

#### Frontend
1. [React](https://reactjs.org/docs/getting-started.html "React")
2. [React Redux](https://redux.js.org/introduction/getting-started "React Redux")
3. [React Semantic UI](https://react.semantic-ui.com/ "React Semantic UI")
4. [Moment.js](https://momentjs.com/ "Moment.js")
5. [validator.js](https://www.npmjs.com/package/validator "validator.js")
6. [history](https://www.npmjs.com/package/history "history")

#### Backend
1. [Node.js](https://nodejs.org/en/ "Node.js")
2. [Express](https://expressjs.com/ru/guide/routing.html "Express")
3. [Passport.js](http://www.passportjs.org/docs/ "Passport.js")
4. [Sequelize](http://docs.sequelizejs.com/ "Sequelize")
5. [axios](https://www.npmjs.com/package/axios "axios")
6. [bcrypt](https://www.npmjs.com/package/bcrypt "bcrypt")
7. [Babel](https://babeljs.io/docs/en/index.html "Babel")
8. [nodemon](https://www.npmjs.com/package/nodemon "nodemon")
9. [dotenv](https://www.npmjs.com/package/dotenv "dotenv")
10. [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken "jsonwebtoken")

#### Database
1. [PostgreSQL](https://www.postgresql.org/download/ "PostgreSQL")


```
npx sequelize-cli db:migrate
npx sequelize-cli db:seed:all
```