/* eslint-disable */
import env from '../env';

export const { protocol, url, port, clientPort } = env.app;
export const { host, mailPort, auth } = env.mail;
