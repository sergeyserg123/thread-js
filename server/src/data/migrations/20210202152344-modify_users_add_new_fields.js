export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'resetCode', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction }),

      queryInterface.addColumn('users', 'isAllowResetPassword', {
        type: Sequelize.STRING,
        defaultValue: false,
        allowNull: false
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'resetCode', { transaction }),
      queryInterface.removeColumn('users', 'isAllowResetPassword', { transaction })
    ]))
};
