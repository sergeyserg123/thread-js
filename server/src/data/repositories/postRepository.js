/* eslint-disable */
import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  PostNegativeReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

// const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
// const dislikeCase = bool => `CASE WHEN "postNegativeReactions"."isDislike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {

    const {
      from: offset,
      count: limit,
      userId,
      userLikedId
    } = filter;

    const where = {};
    let whereLiked = null;
    if (userId) {
      Object.assign(where, { userId });
    }

    if (userLikedId) {
      whereLiked = { userId: userLikedId };
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.literal(`
                        (SELECT COUNT (*)
                        FROM "postReactions" as "reaction"
                        WHERE "post"."id" = "reaction"."postId")`), 'likeCount'],
          [sequelize.literal(`
                        (SELECT COUNT (*)
                        FROM "postNegativeReactions" as "reaction"
                        WHERE "post"."id" = "reaction"."postId")`), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        where: whereLiked,
        attributes: ['id', 'userId'],
        group: ['postReactions.userId'],
        duplicating: false,
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }, {
        model: PostNegativeReactionModel,
        group: ['postNegativeReactions.userId'],
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id',
        'postReactions.id',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'postReactions.id',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.literal(`
                        (SELECT COUNT (*)
                        FROM "postReactions" as "reaction"
                        WHERE "post"."id" = "reaction"."postId")`), 'likeCount'],
          [sequelize.literal(`
                        (SELECT COUNT (*)
                        FROM "postNegativeReactions" as "reaction"
                        WHERE "post"."id" = "reaction"."postId")`), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        include: {
          model: UserModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: ['id'],
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      },
      {
        model: PostNegativeReactionModel,
        attributes: []
      }]
    });
  }

  softDeletePost(id) {
    return this.model.destroy({
      where: { id }
    });
  }
}

export default new PostRepository(PostModel);
