export default (orm, DataTypes) => {
  const User = orm.define('user', {
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    resetCode: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: true
    },
    isAllowResetPassword: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    status: {
      allowNull: true,
      type: DataTypes.STRING
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return User;
};
