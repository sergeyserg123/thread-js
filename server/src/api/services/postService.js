import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import postNegativeReactionRepository from '../../data/repositories/postNegativeReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const update = (postId, post) => postRepository.updateById(postId, post);

export const deletePost = id => postRepository.softDeletePost(id);

export const setReaction = async (userId, { postId, isLike = true }) => {
  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  let dislikeDeleted = false;
  const result = reaction
    ? await postReactionRepository.deleteById(reaction.id)
    : await postReactionRepository.create({ userId, postId, isLike });

  if (!Number.isInteger(result)) {
    const negReaction = await postNegativeReactionRepository.getPostNegativeReaction(userId, postId);
    if (negReaction) {
      await postNegativeReactionRepository.deleteById(negReaction.id);
      dislikeDeleted = true;
    }
  }

  let responseResult = {};

  if (!Number.isInteger(result)) {
    const react = await postReactionRepository.getPostReaction(userId, postId);
    responseResult = { reaction: react, dislikeDeleted };
  }

  return responseResult;
};

export const setNegativeReaction = async (userId, { postId, isDislike = true }) => {
  const reaction = await postNegativeReactionRepository.getPostNegativeReaction(userId, postId);
  let likeDeleted = false;
  const result = reaction
    ? await postNegativeReactionRepository.deleteById(reaction.id)
    : await postNegativeReactionRepository.create({ userId, postId, isDislike });

  if (!Number.isInteger(result)) {
    const posReaction = await postReactionRepository.getPostReaction(userId, postId);
    if (posReaction) {
      await postReactionRepository.deleteById(posReaction.id);
      likeDeleted = true;
    }
  }

  let responseResult = {};

  if (!Number.isInteger(result)) {
    const react = await postNegativeReactionRepository.getPostNegativeReaction(userId, postId);
    responseResult = { reaction: react, likeDeleted };
  }

  return responseResult;
};
