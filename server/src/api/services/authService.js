/* eslint-disable */
import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import { generateResetCode } from '../../helpers/resetCodeHelper';
import userRepository from '../../data/repositories/userRepository';
import * as mailService from './mailService';

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id),
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password),
  });
  return login(newUser);
};

export const resetPassword = async ({ id, email }) => {
  const resetCode = generateResetCode();
  const user = await userRepository.getById(id);

  if (user) {
    const updatedUser = { ...user, resetCode };
    const result = await userRepository.updateById(id, updatedUser);

    if (result) {
      const sent = await mailService.mailSend(email, resetCode);
      if (sent) return sent;
    }
    throw new Error("Could not update user.");
  }
  throw new Error("User not found.");
};

export const confirmResetPassword = async ({ resetCode }) => {
  const user = await userRepository.getByResetCode(resetCode);

  if (user) {
    await userRepository.updateById(user.id, {
      ...user,
      isAllowResetPassword: true,
    });
  }
  return Boolean(user);
};

export const changePassword = async (id, { password }) => {
  const user = await userRepository.getById(id);
  if (user) {
    await userRepository.updateById(id, {
      ...user,
      isAllowResetPassword: false,
      resetCode: null,
      password: await encrypt(password),
    });
  }
  return Boolean(user);
};
