/* eslint-disable */
import nodemailer from 'nodemailer';
import { getResetLink } from '../../helpers/linkHelper';
import * as config from '../../config/mailConfig';

export const mailSend = async (sendTo, resetCode) => {
    const transport = nodemailer.createTransport({
        host: config.host,
        port: config.mailPort,
        auth: config.auth
    });

    const mailOptions = {
        from: '"ThreadJS team"',
        to: `${sendTo}`,
        subject: 'Reset password (ThreadJS)',
        text: `Your try reset the password! For confirmation, click on link: ${getResetLink(resetCode)}`
      };
      
    const info = await transport.sendMail(mailOptions);
    return info ? true : false;
};
