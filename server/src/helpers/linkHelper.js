/* eslint-disable */
import * as config from '../config/mailConfig'

export const getResetLink = resetCode => `${config.protocol}//:${config.url}:${config.clientPort}/profile?resetCode=${resetCode}`;
